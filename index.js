/*Create an activity.js file on where to write and save the solution for the activity.
Create a database of hotel with a collection of rooms.
Insert a single room (insertOne method) with the following details:

- name - single
- accomodates - 2
- price - 1000
- description - A simple room with all the basic necessities
- rooms_available - 10
- isAvailable - false

Insert multiple rooms (insertMany method) with the following details:

- name - double
- accomodates - 3
- price - 2000
- description - A room fit for a small family going on a vacation
- rooms_available - 5
- isAvailable - false

- name - queen
- accomodates - 4
- price - 4000
- description - A room with a queen sized bed perfect for a simple getaway
- rooms_available - 15
- isAvailable - false

Use the find method to search for a room with the name double.
Use the updateOne method to update the queen room and set the available rooms to 0.
Use the deleteMany method rooms to delete all rooms that have 0 availability.
Create a git repository named S28.
Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
Add the link in Boodle.*/


db.rooms.insert({
		name:"single",
		accomodates:2,
		price:1000,
		description:"A simple room with all the basic necessities",
		rooms_available:10,
		isAvailable:false
	})


db.rooms.insertMany([{
		name:"double",
		accomodates:3,
		price:2000,
		description:"A room fit for a small family going on a vacation",
		rooms_available:5,
		isAvailable:false
	},{
		name:"queen",
		accomodates:4,
		price:4000,
		description:"A room with a queen sized bed perfect for a simple getaway",
		rooms_available:15,
		isAvailable:false
	}])


db.rooms.find({name:"double"});

db.rooms.updateOne(
		{name:"queen"},
		{
		$set:{
		name:"queen",
		accomodates:4,
		price:4000,
		description:"A room with a queen sized bed perfect for a simple getaway",
		rooms_available:0,
		isAvailable:false
		}
	}
	);


db.rooms.deleteMany({rooms_available:0});